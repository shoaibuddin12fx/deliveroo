import { Component, OnInit } from '@angular/core';
import { ROUTES } from './fullpage-tab-routes.config';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.scss']
})
export class FullLayoutComponent implements OnInit {

  navLinks: any[] = [];
  constructor() { }

  ngOnInit() {
  }

  configureSidemenu(){
    var routes = ROUTES;

    // configure any routing logics here
    this.navLinks = routes;
  }

}
