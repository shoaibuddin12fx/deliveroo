import { RouteInfo } from 'src/app/shared/routeinfo.metadata';


//Sidebar menu Routes and data

// this will be for future use
// export const EX_ROUTES : RouteInfo[] = []

export const ROUTES: RouteInfo[] = [

    { path: '/pages/home', title: 'Home', icon: 'ft-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [], isActive: false },
    { path: '/pages/jobs', title: 'Jobs', icon: 'ft-bar-chart', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [], isActive: false },
    { path: '/pages/notifications', title: 'Notifications', icon: 'ft-copy', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [], isActive: false },


];
