import { Injectable } from '@angular/core';
import { NetworkService } from '../_helpers/network.service';
import { UtilityService } from '../_helpers/utility.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public network: NetworkService, 
    public utility: UtilityService,) { 

      

  }

  signupUser(email: string, password: string) {
    // your code for signing up the new user
  }



  signinUser(formdata) {
    // your code for checking credentials and getting tokens for for signing in user
    return new Promise( (resolve, reject) => {
      this.network.loginUser(formdata).subscribe( async (data) => {
          if (data['success'] == 'true') {

            const flag = await this.saveData(data);
            if(flag){
              resolve(data);
            }
            
          } else {
            reject(data);
          }

      }, err => {
          console.log(err);
      })

    })

  }

  isAuthenticated() {
    // here you can check if user is authenticated or not through his token
    return new Promise( resolve => {
      resolve(false);
    })

  }

}
