import { Component, OnInit } from '@angular/core';
import { ROUTES } from './footer-routes.config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public menuItems: any[];


  constructor() { }

  ngOnInit() {

    this.configuremenu();
  }

  configuremenu(){
    var routes = ROUTES;

    // configure any routing logics here
    this.menuItems = routes;
  }

}
