import { MatButtonModule, MatSidenavModule, MatSidenavContainer } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


// COMPONENTS


// DIRECTIVES

import { LoginPageComponent } from '../pages/full-pages/login-page/login-page.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { MaterialModule } from '../material.module';


@NgModule({
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
       
        MaterialModule,

    ],
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,


    ],
    declarations: [
        
    ]
})
export class SharedModule { }
