import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ROUTES } from './sidebar-routes.config';
import { MatSidenav } from '@angular/material';
import { SidebarService } from './sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @ViewChild('drawer') public sidenav: MatSidenav;
  public menuItems: any[];

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private sidebar: SidebarService,
    private breakpointObserver: BreakpointObserver) {

    }

  ngOnInit() {

    this.configureSidemenu();
  }

  ngAfterViewInit(): void {

    this.sidebar.setSidenav(this.sidenav);
  }

  configureSidemenu(){
    var routes = ROUTES;

    // configure any routing logics here
    this.menuItems = routes;
  }

}
