import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor() { }

  showLoader() {

  }

  hideLoader() {

  }

  presentSuccessToast(msg) {
    // this.toaster.success(msg)
  }

  presentFailureToast(msg) {
    // this.toaster.error(msg);
  }
}
