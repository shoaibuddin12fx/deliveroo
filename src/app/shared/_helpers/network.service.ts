import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilityService } from './utility.service';
import { ApiService } from './api.service';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'
import { Observable, forkJoin, from } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(public http: HttpClient,
    public utility: UtilityService,
    public api: ApiService) { }


  httpGetUrlResponse(key, id = null, showloader = false) {

    if (showloader == true) {
      this.utility.showLoader();
    }

    const _id = (id) ? '/' + id : ''
    const url = key + _id;
    const seq = this.api.geturl(url)

    seq.subscribe((res: any) => {

      if (showloader == true) {
        this.utility.hideLoader();
      }
      if (res['success'] != true) {
        this.showFailure(res);
      }
    }, err => {
      if (showloader == true) {
        this.utility.hideLoader();
      }
      this.showFailure(err)

    })

    return seq;

  }

  httpGetResponse(key, id = null, showloader = false) {

    if (showloader == true) {
      this.utility.showLoader();
    }

    const _id = (id) ? '/' + id : ''
    const url = key + _id;
    const seq = this.api.get(url)

    seq.subscribe((res: any) => {

      if (showloader == true) {
        this.utility.hideLoader();
      }
      if (res['success'] != true) {
        this.showFailure(res);
      }
    }, err => {
      if (showloader == true) {
        this.utility.hideLoader();
      }
      this.showFailure(err)

    })

    return seq
  }


  showSuccess() {



  }

  showFailure(err) {
    this.utility.presentFailureToast(err['error'])
  }

}
