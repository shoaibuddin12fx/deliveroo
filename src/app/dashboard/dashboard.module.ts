import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";

import { Dashboard1Component } from "./dashboard1/dashboard1.component";
import { Dashboard2Component } from "./dashboard2/dashboard2.component";
import { AuthGuard } from '../shared/auth/auth.guard';


@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
    ],
    exports: [],
    declarations: [
        Dashboard1Component,
        Dashboard2Component
    ],
    providers: [AuthGuard],
})
export class DashboardModule { }
