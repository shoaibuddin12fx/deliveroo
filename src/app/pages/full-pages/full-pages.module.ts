import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullPagesRoutingModule } from './full-pages-routing.module';
import { HomePageComponent } from './home-page/home-page.component';
import { JobsPageComponent } from './jobs-page/jobs-page.component';
import { NotificationsPageComponent } from './notifications-page/notifications-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { TabsPageComponent } from './tabs-page/tabs-page.component';
import { MaterialModule } from 'src/app/material.module';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';




@NgModule({
  entryComponents: [

  ],
  exports: [
    MaterialModule,
    

  ],
  imports: [
    CommonModule,
    FullPagesRoutingModule,
    MaterialModule


  ],
  declarations: [
    HomePageComponent,
    JobsPageComponent,
    NotificationsPageComponent,
    ProfilePageComponent,
    TabsPageComponent,
    
    

  ]
})
export class FullPagesModule { }
