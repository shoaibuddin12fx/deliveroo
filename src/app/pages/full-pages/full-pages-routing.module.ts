import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { JobsPageComponent } from './jobs-page/jobs-page.component';
import { NotificationsPageComponent } from './notifications-page/notifications-page.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'home',
        component: HomePageComponent,
        data: {
          title: 'Home'
        },
        canActivate: [],
      },
      {
        path: 'jobs',
        component: JobsPageComponent,
        data: {
          title: 'Jobs'
        },
        canActivate: [],
      },
      {
        path: 'notifications',
        component: NotificationsPageComponent,
        data: {
          title: 'Notifications'
        },
        canActivate: [],
      },


    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullPagesRoutingModule { }
