import { Component, OnInit, Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BasePage } from '../../base-page';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent extends BasePage implements OnInit {

  aForm: FormGroup;
  submitted = false;

  constructor(injector: Injector, public formBuilder: FormBuilder) {
    super(injector);
    this.setupForm()
  }

  setupForm() {


    this.aForm = this.formBuilder.group({
      email: [{ value: 'shoaib@mailinator.com', disabled: this.submitted}, Validators.compose([Validators.required, Validators.email]) /*, VemailValidator.checkEmail */],
      password: [{ value: '123456', disabled: this.submitted}, Validators.compose([Validators.required])],
    })

  }

  get f() { return this.aForm.controls; }

  ngOnInit() {

  }

  onSubmit(){
    console.log(this.aForm.value);
  }

}
