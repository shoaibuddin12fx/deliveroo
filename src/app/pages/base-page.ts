import { Injector } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';

export abstract class BasePage {

    private activatedRoute: ActivatedRoute;
    public router: Router;
    public location: Location

    constructor(injector: Injector) {
        this.router = injector.get(Router);
        this.activatedRoute = injector.get(ActivatedRoute);
        this.location = injector.get(Location);

        
    }
    
    navigateTo(page: any, queryParams: any = {}) {
        return this.router.navigate([page], { queryParams: queryParams });
    }

    navigateBack() {
        return this.location.back();
    }
}