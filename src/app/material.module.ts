import { NgModule } from '@angular/core';
import { MatSidenavModule, MatTabsModule, MatToolbarModule, MatListModule, MatIconModule, MatCardModule, MatInputModule, MatButtonModule, MatBadgeModule, MatGridListModule, MatFormFieldModule, MatSelectModule, MatRadioModule, MatDatepickerModule, MatChipsModule, MatTooltipModule, MatTableModule, MatPaginatorModule, MatNativeDateModule } from '@angular/material';
import { CommonModule } from '@angular/common';



@NgModule({
    exports: [
      MatButtonModule,
      MatToolbarModule,
      MatIconModule,
      MatSidenavModule,
      MatBadgeModule,
      MatListModule,
      MatGridListModule,
      MatInputModule,
      MatFormFieldModule,
      MatSelectModule,
      MatRadioModule,
      MatDatepickerModule,
      MatChipsModule,
      MatTooltipModule,
      MatTableModule,
      MatPaginatorModule,
      MatFormFieldModule,
      MatInputModule,
      
      

    ],
    imports: [
      CommonModule,
      MatButtonModule,
      MatToolbarModule,
      MatIconModule,
      MatSidenavModule,
      MatBadgeModule,
      MatListModule,
      MatGridListModule,
      MatFormFieldModule,
      MatInputModule,
      MatSelectModule,
      MatRadioModule,
      MatDatepickerModule,
      MatNativeDateModule,
      MatChipsModule,
      MatTooltipModule,
      MatTableModule,
      MatPaginatorModule,
      MatFormFieldModule,
      MatInputModule,
      


    ],
    declarations: [
      
    ],
    providers:[
        MatDatepickerModule,
        MatFormFieldModule,
    ]
})
export class MaterialModule { }